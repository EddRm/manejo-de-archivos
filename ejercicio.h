#ifndef __EJERCICIO__
#define __EJERCICIO__

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include <memory.h>

#include "glthread.h"
#include "config.h"

#define TAM_NOMBRE 			100
#define TAM_NUMEROS 		1000000
#define _TAM_PALABRA_FILE 	100
#define _LIMITE_PALABRAS 	500

typedef struct palabras_ {
    char _palabra[_TAM_PALABRA_FILE];
    glthread_node_t glnode; // Doesn't matter where is it
} palabras_t; 

char *_palabras[_LIMITE_PALABRAS];

int8_t tiene_datos_arch(char *nombre_arch);
int8_t existe_archivo(char *nombre_arch);
FILE *abrir_Archivo_solo_Lectura(char *nombre_arch);
void cerrar_archivo(FILE *ptrArchivo, char *nombre_arch);
int32_t total_palabras_Arch(char *nombre_arch);
void _palabras_TO_Array(char *nombre_arch);
FILE *abrir_Archivo_lectura_escritura(char *nombre_arch);
int crear_archivo(char *nombre_arch);

void print_palabras(palabras_t *palabra);
void crear_nodo_palabra_ll(char palabra_insert[], glthread_t *palabras_list);

void sort(char* arr[], int n);
int mycmp(const char *a, const char *b);

int ejecutar_all(int argc, char const *argv[]);

#endif /* __EJERCICIO__ */