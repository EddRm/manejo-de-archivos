all:
	gcc -g -c -Wall main.c -o main.o
	gcc -g -c -Wall glthread.c -o glthread.o
	gcc -g -c -Wall ejercicio.c -o ejercicio.o
	gcc -g -Wall main.o glthread.o ejercicio.o -o application

clean:
	rm *.o
	rm application
