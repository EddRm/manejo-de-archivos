//Edwin David D�az Ram�rez

#include "ejercicio.h"

static int cmp(const void *p1, const void *p2);

int ejecutar_all(int argc, char const *argv[]){
	FILE *ptrCf = NULL;
	char _file_src[TAM_NOMBRE];
	char _file_dst[TAM_NOMBRE];
	int32_t _total_Palabras_File = 0;

	if(argc != 3){
		printf("Ha olvidado escribir el nombre del primer archivo (1er argumento)\n");
		printf("ó ha olvidado escribir el nombre del segundo archivo (2do argumento)\n");
		printf("\t ---> Intenta de nuevo...\n");
		return 1;
	}
	printf("Desde el programa -->%s, file_src -->%s, file_dst -->%s\n", argv[0], argv[1], argv[2]);		
	for(int32_t _cont = 0; _cont < argc; _cont++){
		printf("argumento[%d] = %s\n", _cont, argv[_cont]);
	}	

	strcpy(_file_src, argv[1]);
	strcpy(_file_dst, argv[2]);

	if(existe_archivo(_file_src)){
#if _DEBUG
		printf("---------------------------------------------------------------\n");
		printf("\tEl archivo -> %s <- EXISTE.\n", _file_src);
		printf("---------------------------------------------------------------\n");
#endif		
		if(tiene_datos_arch(_file_src)){
#if _DEBUG			
			printf("°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°\n");
			printf("\tEL ARCHIVO -> %s <- TIENE DATOS.\n", _file_src);		
			printf("°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°\n");
#endif
			_total_Palabras_File = total_palabras_Arch(_file_src);	
			printf("Total de elementos _file_src [%d]\n\n", _total_Palabras_File);

			_palabras_TO_Array(_file_src);
#if _DEBUG			
			printf("\nLos datos ingresados son: \n");
			for(int32_t i = 0; i < _total_Palabras_File; i++){
		        printf("=>%d, %s\n", i, _palabras[i]);
		    }
#endif
		    sort(_palabras, _total_Palabras_File);

#if _DEBUG
		    printf("<<<---------------------------------------------------------->>>\n");
		    printf("\nLos datos ordenados son: \n");
			for(int32_t i = 0; i < _total_Palabras_File; i++){
		        printf("=>%d, %s\n", i, _palabras[i]);
		    }
#endif	
		    /* Create a glthread */
    		glthread_t *palabras_list = calloc(1, sizeof(glthread_t));
    		init_glthread(palabras_list, offsetof(palabras_t, glnode));

		    for(int i = 0; i < _total_Palabras_File; i++){
		    	crear_nodo_palabra_ll(_palabras[i], palabras_list);
		    }

		    printf("<<<< Palabras en la Lista Enlazada >>>>\n");
		    /* Walk over glthread */
		    printf("```````````````````````````````````````````````````\n");
		    palabras_t *ptr = NULL;
		    ITERATE_GL_THREADS_BEGIN(palabras_list, palabras_t, ptr){
		        print_palabras(ptr);
		    } ITERATE_GL_THREADS_ENDS;
		    printf("```````````````````````````````````````````````````\n");

		    printf(" ---> Nota: Estas mismas palabras deben estar en el archivo [%s]\n", _file_dst);

		    /* Free all Dynamicall allocations */
		    ITERATE_GL_THREADS_BEGIN(palabras_list, palabras_t, ptr){ 
		         glthread_remove(palabras_list, &ptr->glnode);
		         free(ptr);
		    } ITERATE_GL_THREADS_ENDS;
		    free(palabras_list);

		    printf("\n");
			if(existe_archivo(_file_dst)){
#if _DEBUG				
				printf("---------------------------------------------------------------\n");
				printf("\tEl archivo -> %s <- YA EXISTE.\n", _file_dst);
				printf("---------------------------------------------------------------\n");
#endif
			}
			else{
#if _DEBUG				
				printf("---------------------------------------------------------------\n");
				printf("\tEl archivo -> %s <- NO EXISTE.\n", _file_dst);
				printf("---------------------------------------------------------------\n");
#endif
			}						
			if(crear_archivo(_file_dst)){
#if _DEBUG					
				printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
				printf("\tEl archivo -> %s <- Fue creado EXITOSAMENTE.\n", _file_dst);	
				printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
#endif					
				ptrCf = abrir_Archivo_lectura_escritura(_file_dst);
				if(ptrCf == NULL){
#if _DEBUG						
					printf("****************************************************************\n");
					printf("\tEl archivo -> %s <- NO pudo Abrirse.\n", _file_dst);			
					printf("****************************************************************\n");
#endif
				}
				else{
#if _DEBUG						
					printf("****************************************************************\n");
					printf("\tArchivo -> %s <- Abierto Correctamente.\n", _file_dst);			
					printf("****************************************************************\n");		
					printf("----------------------------> ESCRITURA <----------------------------------\n");
#endif							
					int32_t i = 0;				
					while(!feof(stdin)){			
						fprintf(ptrCf, "%s\n", _palabras[i]);
						i++;
						if(i == _total_Palabras_File){
							break;
						}															
					}	
#if _DEBUG														
					printf("\n");
					printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");		
					printf("\tEscritura Terminada.\n");
					printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
#endif						
					cerrar_archivo(ptrCf, _file_dst);
				}	
			}
			else{
#if _DEBUG					
				printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
				printf("\tEl archivo -> %s <- NO pudo crearse.\n", _file_dst);			
				printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
#endif					
			}
		}
		else{
#if _DEBUG			
			printf("°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°\n");
			printf("\tEL ARCHIVO -> %s <- NO TIENE DATOS.\n", _file_src);
			printf("°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°\n");
#endif			
		}
	}
	else{
#if _DEBUG		
		printf("---------------------------------------------------------------\n");
		printf("\tEl archivo -> %s <- NO EXISTE.\n", _file_src);
		printf("---------------------------------------------------------------\n");
#endif
	}
	return 0;
}

int8_t tiene_datos_arch(char *nombre_arch){
	int8_t hay_Datos = 0;
	FILE *ptrArchivo;
	int32_t contador = 0;
	char _linea[_TAM_PALABRA_FILE];
	ptrArchivo = abrir_Archivo_solo_Lectura(nombre_arch);
	if(ptrArchivo == NULL){
#if _DEBUG		
		printf("El archivo -> %s <- NO se pudo abrir desde tiene_datos_arch().\n", nombre_arch);
#endif			
		hay_Datos = 0;
	}
	else{
#if _DEBUG		
		printf("Archivo -> %s <- abierto desde tiene_datos_arch().\n", nombre_arch);
#endif
		fscanf(ptrArchivo, "%s", _linea);
		// mientras no sea fin de archivo.
		while(!feof(ptrArchivo)){
#if _DEBUG			
			printf(" --> Linea %d = [%s]\n", contador, _linea);
#endif
			contador++;
			// leo si hay datos
			fscanf(ptrArchivo, "%s", _linea);
			if(contador > 0){
				hay_Datos = 1;
				break;
			}
		}
		cerrar_archivo(ptrArchivo, nombre_arch);
	}	
	return hay_Datos;
}

int8_t existe_archivo(char *nombre_arch){
	FILE *ptrArchivo;
	int8_t existe = 0;
	// Con r abre un archivo para lectura.
	ptrArchivo = fopen(nombre_arch, "r");
	if(ptrArchivo != NULL){
		existe = 1;
		cerrar_archivo(ptrArchivo, nombre_arch);
	}	
	return existe;
}

FILE *abrir_Archivo_solo_Lectura(char *nombre_arch){
	FILE *ptrArchivo;
	ptrArchivo = fopen(nombre_arch, "r");
	if(ptrArchivo == NULL){
#if _DEBUG		
		// con r Abre un archivo para lectura.
		printf("---> El archivo -> %s <- NO pudo abrirse (uso de r).\n", nombre_arch);
#endif
	}	
	else{
#if _DEBUG		
		printf("---> Archivo -> %s <- Abierto (uso de r).\n", nombre_arch);
#endif
	}
	return ptrArchivo;
}

void cerrar_archivo(FILE *ptrArchivo, char *nombre_arch){
	fclose(ptrArchivo);
#if _DEBUG		
	printf("----> Archivo -> %s <- Cerrado Correctamente.\n", nombre_arch);
#endif	
}

int32_t total_palabras_Arch(char *nombre_arch){
	int32_t contador = 0;
	char _palabra[_TAM_PALABRA_FILE];
	FILE *ptrArchivo;
	ptrArchivo = abrir_Archivo_solo_Lectura(nombre_arch);
	if(ptrArchivo == NULL){
#if _DEBUG			
		printf("El archivo -> %s <- NO se pudo abrir desde total_palabras_Arch().\n", nombre_arch);					
#endif
	}
	else{
#if _DEBUG			
		printf("Archivo -> %s <- abierto desde total_palabras_Arch().\n", nombre_arch);
#endif
		fscanf(ptrArchivo, "%s", _palabra);
		// mientras no sea fin de archivo.
		while(!feof(ptrArchivo)){
			contador++;			
			// leo si hay datos
			fscanf(ptrArchivo, "%s", _palabra);
		}
		cerrar_archivo(ptrArchivo, nombre_arch);
	}
	return (contador);
}

void _palabras_TO_Array(char *nombre_arch){
	int32_t contador = 0;
	FILE *ptrArchivo;
	char _dato[_TAM_PALABRA_FILE];
	ptrArchivo = abrir_Archivo_solo_Lectura(nombre_arch);
	if(ptrArchivo == NULL){
#if _DEBUG			
		printf("El archivo -> %s <- NO se pudo abrir desde _palabras_TO_Array().\n", nombre_arch);					
#endif		
	}
	else{
#if _DEBUG			
		printf("Archivo -> %s <- abierto desde _palabras_TO_Array().\n", nombre_arch);
#endif		
		fscanf(ptrArchivo, "%s", _dato);
		// mientras no sea fin de archivo.
		while(!feof(ptrArchivo)){
#if _DEBUG	
			printf(" --> _dato %d = [%s]\n", contador, _dato);
#endif				
			_palabras[contador] = (char *)malloc(sizeof(char)*_TAM_PALABRA_FILE);
			strcpy(_palabras[contador], _dato);
			contador++;			
			// leo si hay datos
			fscanf(ptrArchivo, "%s", _dato);
		}
		cerrar_archivo(ptrArchivo, nombre_arch);
	}
}

FILE *abrir_Archivo_lectura_escritura(char *nombre_arch){
	FILE *ptrArchivo;
	ptrArchivo = fopen(nombre_arch, "r+");
	if( ptrArchivo == NULL ){
#if _DEBUG			
		// con r+ Abre un archivo para actualización (lectura y escritura).
		printf("---> El archivo -> %s <- NO pudo abrirse (uso de r+).\n", nombre_arch);
#endif		
	}	
	else{
#if _DEBUG			
		printf("---> Archivo -> %s <- Abierto (uso de r+).\n", nombre_arch);
#endif		
	}
	return ptrArchivo;
}

int crear_archivo(char *nombre_arch){
	int creado = 0;
	FILE *ptrArchivo;
	ptrArchivo = fopen(nombre_arch, "w");
	if( ptrArchivo == NULL ){
#if _DEBUG			
		// con w crea un archivo para escritura. Si el archivo ya existe, descarta el contenido actual.
		printf("----> El archivo -> %s <- no pudo crearse (abrir; uso de w).\n", nombre_arch);
#endif		
	}	
	else{
		creado = 1;
#if _DEBUG			
		printf("----> Archivo -> %s <- Creado (abierto; uso de w).\n", nombre_arch);
#endif		
		cerrar_archivo(ptrArchivo, nombre_arch);
	}
	return creado;
}

/*******************************************************************************/

void print_palabras(palabras_t *palabra){
    printf("palabra = %s\n", palabra->_palabra);
}

void crear_nodo_palabra_ll(char palabra_insert[], glthread_t *palabras_list){
	palabras_t *palabra  = calloc(1, sizeof(palabras_t));
	strncpy(palabra->_palabra, palabra_insert, strlen(palabra_insert));
	glthread_node_init((&palabra->glnode));

	/* Now insert the records in glthread */
	glthread_add(palabras_list, &palabra->glnode);
}

// --------------
int mycmp(const char *a, const char *b){
	const char *cp1 = a, *cp2 = b;

    for (; toupper(*cp1) == toupper(*cp2); cp1++, cp2++)
        if (*cp1 == '\0')
            return 0;
    return ((toupper(*cp1) < toupper(*cp2)) ? -1 : +1);
} 

// Function to sort the array 
void sort(char* arr[], int n){ 
    // calling qsort function to sort the array 
    // with the help of Comparator 
    qsort(arr, n, sizeof(const char*), cmp);
} 

// --------------

static int cmp(const void *p1, const void *p2){
    return mycmp(* (char * const *) p1, * (char * const *) p2);
}

// --------------
